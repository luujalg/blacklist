package com.lhvtestassignment.blacklist.rating;

import com.lhvtestassignment.blacklist.MatchItem;
import com.lhvtestassignment.blacklist.filter.MinimumJaroWinklerFilter;
import com.lhvtestassignment.blacklist.filter.RequiredNamePartsFilter;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.text.similarity.JaroWinklerSimilarity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Predicate;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@AllArgsConstructor
@Component
public class MatchRatingProvider {

  @Value("${blacklist.matcher.minimumJaroWinklerValuePerNamePart}")
  private Double minimumJaroWinklerValueToMatch;

  @Value("${blacklist.matcher.resultSizeLimit}")
  private int resultSizeLimit;

  private final Logger LOGGER = LoggerFactory.getLogger(MatchRatingProvider.class);
  private final JaroWinklerSimilarity jaroWinklerSimilarity;

  public MatchRatingProvider() {
    jaroWinklerSimilarity = new JaroWinklerSimilarity();
  }

  public List<MatchResult> findBestMatches(List<MatchItem> matchItems, String input) {
    LOGGER.info("Calculating ratings for MatchItems");
    Map<MatchItem, MatchRating> ratingsByMatch = new HashMap<>();
    matchItems.forEach(item -> ratingsByMatch.put(item, getRating(item)));
    Map<MatchItem, MatchRating> filteredMatches = filterMatches(ratingsByMatch);
    List<MatchResult> results = createMatchResults(filteredMatches);

    return !results.isEmpty() ? results : singletonList(new MatchResult(input, null, null));
  }

  private MatchRating getRating(MatchItem item) {
    Map<String, String> namePhoneticCodesByNamePart = item.getNamePhoneticCodesByNamePart();
    Map<String, List<String>> inputPartsByPhoneticCode = item.getInputPartsByPhoneticCode();

    MatchRating rating = new MatchRating();
    namePhoneticCodesByNamePart.forEach((namePart, phoneticCode) -> {
      List<String> inputParts = inputPartsByPhoneticCode.getOrDefault(phoneticCode, emptyList());
      if (!inputParts.isEmpty()) {
        Optional<Pair<String, Double>> inputPartRating = getBestMatchingInputPart(namePart, inputParts);
        inputPartRating.ifPresent(inputRating -> rating.addRating(namePart, inputRating));
      }
    });

    return rating;
  }

  private Optional<Pair<String, Double>> getBestMatchingInputPart(String namePart, List<String> inputParts) {
    return inputParts.stream()
        .map(inputPart -> {
          Double rating = jaroWinklerSimilarity.apply(namePart.toLowerCase(), inputPart.toLowerCase());
          return Pair.of(inputPart, rating);
        })
        .max(Comparator.comparing(Pair::getRight));
  }

  private Map<MatchItem, MatchRating> filterMatches(Map<MatchItem, MatchRating> ratingsByMatch) {
    LOGGER.info("Applying filters to match ratings");
    List<Predicate<Map.Entry<MatchItem, MatchRating>>> filters = createFilters();
    return ratingsByMatch.entrySet().stream()
        .filter(filters.stream().reduce(filter -> true, Predicate::and))
        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  private List<Predicate<Map.Entry<MatchItem, MatchRating>>> createFilters() {
    return List.of(
        new RequiredNamePartsFilter(),
        new MinimumJaroWinklerFilter(minimumJaroWinklerValueToMatch)
    );
  }

  private List<MatchResult> createMatchResults(Map<MatchItem, MatchRating> ratingsByMatch) {
    LOGGER.info("Creating match results");
    return ratingsByMatch.entrySet().stream()
        .map(entry -> new MatchResult(entry.getKey(), entry.getValue()))
        .sorted(Comparator.comparing(MatchResult::getAggregateScore).reversed())
        .limit(resultSizeLimit)
        .collect(toList());
  }

}
