package com.lhvtestassignment.blacklist.rating;

import com.lhvtestassignment.blacklist.MatchItem;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.DoubleStream;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@RequiredArgsConstructor
@Getter
public class MatchResult {

  private final String input;
  private final String matchedName;
  private final Double aggregateScore;

  public MatchResult(MatchItem matchItem, MatchRating matchRating) {
    input = matchItem.getInput();
    matchedName = matchItem.getName();
    this.aggregateScore = calculateAggregateRating(matchItem, matchRating);
  }

  private Double calculateAggregateRating(MatchItem matchItem, MatchRating matchRating) {
    String firstName = matchItem.getFirstName();
    String lastName = matchItem.getLastName();
    return DoubleStream.of(matchRating.getRating(firstName), matchRating.getRating(lastName))
        .average()
        .orElse(0);
  }

  public Double getAggregateScore() {
    return aggregateScore;
  }

  @Override
  public String toString() {
    if (isEmpty(matchedName)) {
      return String.format("INPUT: %s\nNo matches found!\n", input);
    }

    return String.format("INPUT: %s\nMATCHED NAME: %s\nSCORE: %.2f\n", input, matchedName, aggregateScore);
  }
}
