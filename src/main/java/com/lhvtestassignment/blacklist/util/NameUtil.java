package com.lhvtestassignment.blacklist.util;

import org.apache.commons.codec.language.DoubleMetaphone;

public class NameUtil {

  private static final DoubleMetaphone doubleMetaphone = new DoubleMetaphone();

  public static String getPhoneticCode(String input) {
    return doubleMetaphone.doubleMetaphone(input);
  }
}
