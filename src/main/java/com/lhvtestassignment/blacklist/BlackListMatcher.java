package com.lhvtestassignment.blacklist;

import com.lhvtestassignment.blacklist.data.BlackListDataHolder;
import com.lhvtestassignment.blacklist.data.NameItem;
import com.lhvtestassignment.blacklist.rating.MatchRatingProvider;
import com.lhvtestassignment.blacklist.rating.MatchResult;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
@Component
public class BlackListMatcher {

  private final Logger LOGGER = LoggerFactory.getLogger(BlackListDataHolder.class);

  private final BlackListDataHolder blackListDataHolder;
  private final MatchRatingProvider ratingProvider;

  public List<MatchResult> findMatch(String input) {
    List<MatchItem> phoneticMatches = findPhoneticMatches(createInputItem(input));
    if (phoneticMatches.isEmpty()) {
      LOGGER.info(String.format("No matches found for: %s%n", input));
      return singletonList(new MatchResult(input, null, null));
    }

    List<MatchItem> itemsWithMaxMatchingCodes = getMatchItemsWithMostCommonPhoneticCodes(phoneticMatches);
    return ratingProvider.findBestMatches(itemsWithMaxMatchingCodes, input);
  }

  private InputItem createInputItem(String input) {
    List<String> inputParts = getInputAsParts(input.toLowerCase().trim());
    cleanInput(inputParts);
    return new InputItem(input, inputParts);
  }

  private void cleanInput(List<String> input) {
    input.removeAll(blackListDataHolder.getNoiseWords());
  }

  private List<String> getInputAsParts(String input) {
    if (StringUtils.isBlank(input)) {
      return emptyList();
    }

    return new ArrayList<>(List.of(input.split("\\P{Alpha}+")));
  }

  private List<MatchItem> findPhoneticMatches(InputItem inputItem) {
    LOGGER.info("Finding phonetic matches");
    List<String> inputPhoneticCodes = inputItem.getInputPhoneticCodes();
    Set<NameItem> matchingNames = new HashSet<>();
    inputPhoneticCodes.forEach(value -> matchingNames.addAll(blackListDataHolder.getNames(value)));
    return matchingNames.stream()
        .map(item -> new MatchItem(inputItem, item))
        .collect(toList());
  }

  private List<MatchItem> getMatchItemsWithMostCommonPhoneticCodes(List<MatchItem> matchItems) {
    LOGGER.info("Finding MatchItems with the largest amount of common phonetic codes");
    Map<MatchItem, Integer> matchingCodesPerName = getNumberOfCommonPhoneticCodesByMatchItem(matchItems);
    Integer maximumMatchingCodes = Collections.max(matchingCodesPerName.values());
    return matchingCodesPerName.entrySet().stream()
        .filter(entry -> entry.getValue().equals(maximumMatchingCodes))
        .map(Map.Entry::getKey)
        .collect(toList());
  }

  private Map<MatchItem, Integer> getNumberOfCommonPhoneticCodesByMatchItem(List<MatchItem> matchItems) {
    Map<MatchItem, Integer> matchingCodesPerName = new HashMap<>();
    matchItems.forEach(item -> matchingCodesPerName.put(item, item.getCommonPhoneticCodes().size()));
    return matchingCodesPerName;
  }

}
