package com.lhvtestassignment.blacklist;

import com.lhvtestassignment.blacklist.data.BlackListDataHolder;
import com.lhvtestassignment.blacklist.rating.MatchRatingProvider;
import com.lhvtestassignment.blacklist.rating.MatchResult;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BlackListMatcherTest {

  private static BlackListMatcher blackListMatcher;
  private static BlackListDataHolder dataHolder;

  @BeforeAll
  public static void setUp() {
    dataHolder = mock(BlackListDataHolder.class);
    MatchRatingProvider ratingProvider = mock(MatchRatingProvider.class);
    blackListMatcher = new BlackListMatcher(dataHolder, ratingProvider);
  }

  @Test
  public void noPhoneticMatchesReturnsEmptyResult() {
    when(dataHolder.getNoiseWords()).thenReturn(emptyList());
    when(dataHolder.getNames(any(String.class))).thenReturn(emptyList());
    List<MatchResult> result = blackListMatcher.findMatch("");
    assertTrue(result.size() == 1 && StringUtils.isBlank(result.get(0).getMatchedName()));
  }

}