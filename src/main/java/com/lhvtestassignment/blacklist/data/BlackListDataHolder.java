package com.lhvtestassignment.blacklist.data;

import com.lhvtestassignment.blacklist.util.FileUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.language.DoubleMetaphone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class BlackListDataHolder {

  @Value("${blacklist.names}")
  private String namesFile;

  @Value("${blacklist.noiseWords}")
  private String noiseWordsFile;

  private final int MIN_NAME_PARTS = 2;

  private final Logger LOGGER = LoggerFactory.getLogger(BlackListDataHolder.class);
  private final DoubleMetaphone doubleMetaphone = new DoubleMetaphone();

  private final Map<String, List<NameItem>> namesByPhoneticToken = new HashMap<>();
  private List<String> noiseWords;

  @PostConstruct
  public void initialize() {
    LOGGER.info(String.format("Blacklist names file: %s", namesFile));
    LOGGER.info(String.format("Noise words file: %s", noiseWordsFile));

    noiseWords = getNoiseWordsFromFile();
    List<String> names = FileUtil.getFileLines(namesFile);
    createMetaPhoneCodes(names);
  }

  private List<String> getNoiseWordsFromFile() {
    LOGGER.info("Getting noise words from file");
    List<String> lines = FileUtil.getFileLines(noiseWordsFile);
    return lines.stream()
        .map(String::toLowerCase)
        .collect(toList());
  }

  private void createMetaPhoneCodes(List<String> names) {
    LOGGER.info("Creating phonetic codes for blacklist names");
    names.forEach(name -> {
      NameItem nameItem = createNameItem(name);
      if (nameItem == null) {
        return;
      }

      List<String> namePhoneticCodes = nameItem.getPhoneticCodes();
      namePhoneticCodes.forEach(code -> {
        List<NameItem> nameItems = namesByPhoneticToken.getOrDefault(code, new ArrayList<>());
        nameItems.add(nameItem);
        namesByPhoneticToken.put(code, nameItems);
      });
    });
  }

  private NameItem createNameItem(String name) {
    List<String> nameParts = Arrays.asList(name.split("\\P{Alpha}+"));
    if (nameParts.size() < MIN_NAME_PARTS) {
      String errorMsg = String.format("%s is not a valid name! Must have at least a first and a last name!", name);
      throw new IllegalStateException(errorMsg);
    }

    return NameItem.builder()
        .fullName(name)
        .firstName(nameParts.get(0))
        .lastName(nameParts.get(nameParts.size() - 1))
        .middleNames(nameParts.subList(1, nameParts.size() - 1))
        .phoneticCodesByNamePart(createCodes(name))
        .build();
  }

  private Map<String, String> createCodes(String name) {
    List<String> nameParts = Arrays.asList(name.split("\\P{Alpha}+"));
    Map<String, String> phoneticCodesByNamePart = new HashMap<>();
    nameParts.forEach(part -> phoneticCodesByNamePart.put(part, doubleMetaphone.doubleMetaphone(part)));
    return phoneticCodesByNamePart;
  }

  public List<String> getNoiseWords() {
    return noiseWords;
  }

  public List<NameItem> getNames(String phoneticCode) {
    return namesByPhoneticToken.getOrDefault(phoneticCode, emptyList());
  }

}
