package com.lhvtestassignment.blacklist.data;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Getter
@Builder
public class NameItem {

  private final String fullName;
  private final String firstName;
  private final String lastName;
  private final List<String> middleNames;

  private final Map<String, String> phoneticCodesByNamePart;

  public List<String> getPhoneticCodes() {
    return List.copyOf(phoneticCodesByNamePart.values());
  }

}
